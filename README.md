# nDisplay

Unreal Engine nDisplay plugin extended with Linux support. See https://wiki.unrealengine.com/Override_Built_In_Plugin for adding it to your project.