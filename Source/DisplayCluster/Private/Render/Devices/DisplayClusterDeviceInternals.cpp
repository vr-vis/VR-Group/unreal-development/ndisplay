// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "DisplayClusterDeviceInternals.h"


#if PLATFORM_WINDOWS
PFNWGLSWAPINTERVALEXTPROC    DisplayCluster_wglSwapIntervalEXT_ProcAddress   = nullptr;

PFNWGLJOINSWAPGROUPNVPROC      DisplayCluster_wglJoinSwapGroupNV_ProcAddress   = nullptr;
PFNWGLBINDSWAPBARRIERNVPROC    DisplayCluster_wglBindSwapBarrierNV_ProcAddress = nullptr;
PFNWGLQUERYSWAPGROUPNVPROC     DisplayCluster_wglQuerySwapGroupNV_ProcAddress = nullptr;
PFNWGLQUERYMAXSWAPGROUPSNVPROC DisplayCluster_wglQueryMaxSwapGroupsNV_ProcAddress = nullptr;
PFNWGLQUERYFRAMECOUNTNVPROC    DisplayCluster_wglQueryFrameCountNV_ProcAddress = nullptr;
PFNWGLRESETFRAMECOUNTNVPROC    DisplayCluster_wglResetFrameCountNV_ProcAddress = nullptr;


// Copy/pasted from OpenGLDrv.cpp
static void DisplayClusterGetExtensionsString(FString& ExtensionsString)
{
	GLint ExtensionCount = 0;
	ExtensionsString = TEXT("");
	if (FOpenGL::SupportsIndexedExtensions())
	{
		glGetIntegerv(GL_NUM_EXTENSIONS, &ExtensionCount);
		for (int32 ExtensionIndex = 0; ExtensionIndex < ExtensionCount; ++ExtensionIndex)
		{
			const ANSICHAR* ExtensionString = FOpenGL::GetStringIndexed(GL_EXTENSIONS, ExtensionIndex);

			ExtensionsString += TEXT(" ");
			ExtensionsString += ANSI_TO_TCHAR(ExtensionString);
		}
	}
	else
	{
		const ANSICHAR* GlGetStringOutput = (const ANSICHAR*)glGetString(GL_EXTENSIONS);
		if (GlGetStringOutput)
		{
			ExtensionsString += GlGetStringOutput;
			ExtensionsString += TEXT(" ");
		}
	}
}

// https://www.opengl.org/wiki/Load_OpenGL_Functions
static void* DisplayClusterGetGLFuncAddress(const char *name)
{
	HMODULE module = LoadLibraryA("opengl32.dll");
	if (module)
	{
		return (void *)GetProcAddress(module, name);
	}
	else
	{
		return nullptr;
	}
}

// Copy/pasted from OpenGLDevice.cpp
// static void InitRHICapabilitiesForGL()
void DisplayClusterInitCapabilitiesForGL()
{
	bool bWindowsSwapControlExtensionPresent = false;
	{
		FString ExtensionsString;
		DisplayClusterGetExtensionsString(ExtensionsString);

		if (ExtensionsString.Contains(TEXT("WGL_EXT_swap_control")))
		{
			bWindowsSwapControlExtensionPresent = true;
		}
	}

#pragma warning(push)
#pragma warning(disable:4191)
	if (bWindowsSwapControlExtensionPresent)
	{
		DisplayCluster_wglSwapIntervalEXT_ProcAddress = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
	}

	DisplayCluster_wglJoinSwapGroupNV_ProcAddress      = (PFNWGLJOINSWAPGROUPNVPROC)wglGetProcAddress("wglJoinSwapGroupNV");
	DisplayCluster_wglBindSwapBarrierNV_ProcAddress    = (PFNWGLBINDSWAPBARRIERNVPROC)wglGetProcAddress("wglBindSwapBarrierNV");
	DisplayCluster_wglQuerySwapGroupNV_ProcAddress     = (PFNWGLQUERYSWAPGROUPNVPROC)wglGetProcAddress("wglQuerySwapGroupNV");
	DisplayCluster_wglQueryMaxSwapGroupsNV_ProcAddress = (PFNWGLQUERYMAXSWAPGROUPSNVPROC)wglGetProcAddress("wglQueryMaxSwapGroupsNV");
	DisplayCluster_wglQueryFrameCountNV_ProcAddress    = (PFNWGLQUERYFRAMECOUNTNVPROC)wglGetProcAddress("wglQueryFrameCountNV");
	DisplayCluster_wglResetFrameCountNV_ProcAddress    = (PFNWGLRESETFRAMECOUNTNVPROC)wglGetProcAddress("wglResetFrameCountNV");

#pragma warning(pop)
}
#endif



#if PLATFORM_LINUX
GLX_JoinSwapGroupNV_Func DisplayCluster_glXJoinSwapGroupNV_ProcAddress = nullptr;
GLX_BindSwapBarrierNV_Func DisplayCluster_glXBindSwapBarrierNV_ProcAddress = nullptr;
GLX_QuerySwapGroupNV_Func DisplayCluster_glXQuerySwapGroupNV_ProcAddress = nullptr;
GLX_QueryMaxSwapGroupsNV_Func DisplayCluster_glXQueryMaxSwapGroupsNV_ProcAddress = nullptr;
GLX_QueryFrameCountNV_Func DisplayCluster_glXQueryFrameCountNV_ProcAddress = nullptr;
GLX_ResetFrameCountNV_Func DisplayCluster_glXResetFrameCountNV_ProcAddress = nullptr;

void DisplayClusterInitCapabilitiesForGL()
{
	DisplayCluster_glXJoinSwapGroupNV_ProcAddress 			= (GLX_JoinSwapGroupNV_Func) SDL_GL_GetProcAddress("glXJoinSwapGroupNV");
	DisplayCluster_glXBindSwapBarrierNV_ProcAddress 		= (GLX_BindSwapBarrierNV_Func) SDL_GL_GetProcAddress("glXBindSwapBarrierNV");
	DisplayCluster_glXQuerySwapGroupNV_ProcAddress 			= (GLX_QuerySwapGroupNV_Func) SDL_GL_GetProcAddress("glXQuerySwapGroupNV");
	DisplayCluster_glXQueryMaxSwapGroupsNV_ProcAddress 	= (GLX_QueryMaxSwapGroupsNV_Func) SDL_GL_GetProcAddress("glXQueryMaxSwapGroupsNV");
	DisplayCluster_glXQueryFrameCountNV_ProcAddress 		= (GLX_QueryFrameCountNV_Func) SDL_GL_GetProcAddress("glXQueryFrameCountNV");
	DisplayCluster_glXResetFrameCountNV_ProcAddress 		= (GLX_ResetFrameCountNV_Func) SDL_GL_GetProcAddress("glXResetFrameCountNV");
}
#endif
