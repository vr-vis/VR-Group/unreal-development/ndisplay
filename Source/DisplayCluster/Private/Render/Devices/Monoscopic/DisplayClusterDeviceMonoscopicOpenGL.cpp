// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "DisplayClusterDeviceMonoscopicOpenGL.h"
#include "Render/Devices/DisplayClusterDeviceInternals.h"

#include "Misc/DisplayClusterLog.h"



FDisplayClusterDeviceMonoscopicOpenGL::FDisplayClusterDeviceMonoscopicOpenGL()
{

}

FDisplayClusterDeviceMonoscopicOpenGL::~FDisplayClusterDeviceMonoscopicOpenGL()
{

}

bool FDisplayClusterDeviceMonoscopicOpenGL::NeedReAllocateViewportRenderTarget(const class FViewport& Viewport)
{
	return FDisplayClusterDeviceBase::NeedReAllocateViewportRenderTarget(Viewport);
}

void FDisplayClusterDeviceMonoscopicOpenGL::CalculateRenderTargetSize(const class FViewport& Viewport, uint32& InOutSizeX, uint32& InOutSizeY)
{
	FDisplayClusterDeviceBase::CalculateRenderTargetSize(Viewport, InOutSizeX, InOutSizeY);
}

void FDisplayClusterDeviceMonoscopicOpenGL::AdjustViewRect(enum EStereoscopicPass StereoPass, int32& X, int32& Y, uint32& SizeX, uint32& SizeY) const
{
	FDisplayClusterDeviceBase::AdjustViewRect(StereoPass, X, Y, SizeX, SizeY);
}

EStereoscopicPass FDisplayClusterDeviceMonoscopicOpenGL::GetViewPassForIndex(bool bStereoRequested, uint32 ViewIndex) const
{
	if (ViewIndex == 0)
		return EStereoscopicPass::eSSP_MONOSCOPIC_EYE;
	else if (ViewIndex == 1)
		return EStereoscopicPass::eSSP_LEFT_EYE;
	else
		return EStereoscopicPass::eSSP_FULL;
}

bool FDisplayClusterDeviceMonoscopicOpenGL::IsStereoEyePass(EStereoscopicPass Pass)
{
	return Pass != EStereoscopicPass::eSSP_FULL;
}

bool FDisplayClusterDeviceMonoscopicOpenGL::Present(int32& InOutSyncInterval)
{
	UE_LOG(LogDisplayClusterRender, Verbose, TEXT("FDisplayClusterDeviceQuadBufferStereoOpenGL::Present"));

	const int dstX1 = 0;
	const int dstX2 = BackBuffSize.X;

	// Convert to left bottom origin and flip Y
	const int dstY1 = ViewportSize.Y;
	const int dstY2 = 0;

	FOpenGLViewport* pOglViewport = static_cast<FOpenGLViewport*>(CurrentViewport->GetViewportRHI().GetReference());
	check(pOglViewport);
	FPlatformOpenGLContext* const pContext = pOglViewport->GetGLContext();

#if PLATFORM_WINDOWS
	check(pContext && pContext->DeviceContext);
#elif PLATFORM_LINUX
	check(pContext && pContext->hWnd);
#endif

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glDrawBuffer(GL_BACK);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, pContext->ViewportFramebuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT0);

	glBlitFramebuffer(
		0, 0, BackBuffSize.X, BackBuffSize.Y,
		dstX1, dstY1, dstX2, dstY2,
		GL_COLOR_BUFFER_BIT,
		GL_NEAREST);
	
	// Perform buffers swap logic
	SwapBuffers(pOglViewport, InOutSyncInterval);
	REPORT_GL_END_BUFFER_EVENT_FOR_FRAME_DUMP();

	return false;
}
