// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Render/Devices/QuadBufferStereo/DisplayClusterDeviceQuadBufferStereoOpenGL.h"
#include "Render/Devices/DisplayClusterDeviceInternals.h"


/**
 * Monoscopic emulation device (OpenGL 3 and 4)
 */
class FDisplayClusterDeviceMonoscopicOpenGL : public FDisplayClusterDeviceQuadBufferStereoOpenGL
{
public:
	FDisplayClusterDeviceMonoscopicOpenGL();
	virtual ~FDisplayClusterDeviceMonoscopicOpenGL();

public:
	virtual bool ShouldUseSeparateRenderTarget() const override
	{ return false; }

	virtual bool NeedReAllocateViewportRenderTarget(const class FViewport& Viewport) override;	
	virtual void CalculateRenderTargetSize(const class FViewport& Viewport, uint32& InOutSizeX, uint32& InOutSizeY) override;

public:
	//////////////////////////////////////////////////////////////////////////////////////////////
	// IStereoRendering
	//////////////////////////////////////////////////////////////////////////////////////////////
	virtual int32 GetDesiredNumberOfViews(bool bStereoRequested) const override
	{ return 1; }

	virtual void AdjustViewRect(enum EStereoscopicPass StereoPass, int32& X, int32& Y, uint32& SizeX, uint32& SizeY) const override;
	virtual EStereoscopicPass GetViewPassForIndex(bool bStereoRequested, uint32 ViewIndex) const override;
	virtual bool IsStereoEyePass(EStereoscopicPass Pass) override;

protected:
	virtual bool Present(int32& InOutSyncInterval) override;
};
